document.addEventListener("DOMContentLoaded",  function () {
	"use strict";

	// forms data
    var settings = (typeof settings == 'Array')? settings : [];


	// example
	settings.push({
		id: 'form-enroll',
		title: 'Записаться онлайн',
		action: 'mail',
		actionData: {
			'email': '*******@gmail.com',
			'title': 'Запись с сайта',
		},
		selector: {
			click: ['.plashka1']
		},
		form: {
			select:[
				{
					id: 'customer-city',
					name: 'form[city]',
					init: function(element){
						for(var i = 0; i < document.querySelectorAll('#mini-menu > ul > li > a').length; ++i){
							element.options[i] = new Option(document.querySelectorAll('#mini-menu > ul > li > a')[i].textContent);
						}
					},
					onChange: function(element){

					}
				},
			],
			input: [
				{
					id: 'customer-name',
					name: 'form[name]',
					type: 'text',
					required: ""
				},
			],
			label:[
				{
					for: 'customer-name',
					init: function(element){
						var text = document.createTextNode('Введите Вашe имя *:');
						element.appendChild(text);
					}
				},

			],
			textarea:[
				{
					id: 'customer-comments',
					name: 'form[comments]'
				}
			]
		}
	});

	// execute script
	var forms = [];
	for(var i = 0; i < settings.length; ++i){
		var form = new Form(settings[i]);
		form.init();
		form.addEvents();
		forms[i] = form;
	}

	function Form(config){
		var self = this;

		this.config = config;

		this.formElement;

		this.wrapper;

		this.init = function(){
			// creating and adding Form element
			this.formElement = document.createElement('form');
			this.formElement.className = "ss-form";
			this.formElement.id = this.config.id;
			this.formElement.style.display = 'none';
			document.querySelector('body').appendChild(this.formElement);

			// add wrapper
			if(!document.getElementById('ss-form-wrapper')){
				this.wrapper = document.createElement( "div" );
				this.wrapper.id = 'ss-form-wrapper';
				document.getElementsByTagName( "body" )[0].insertBefore( this.wrapper,  document.getElementsByTagName( "div" )[0]);
			}

			// add styles
			var href = this.config.style ? this.config.style : '/feedback/css/default.css';
			if(!document.querySelector('link[href="'+ href +'"]')){
				var link = document.createElement( "link" );
				link.href = href;
				link.type = "text/css";
				link.rel = "stylesheet";
				document.getElementsByTagName( "head" )[0].appendChild( link );
			}

			// add form title
			var title = document.createElement( "div" );
			var text = document.createTextNode(this.config.title);
			title.className = "style-h2";
			title.appendChild(text);
			this.formElement.appendChild(title);

			// add close button
			var svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
			svg.setAttribute('viewBox', "0 0 102 102");
			var polygon = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
			polygon.setAttribute('fill', "#AAAAAB");
			polygon.setAttribute('points', '96,14 82,0 48,34 14,0 0,14 34,48 0,82 14,96 48,62 82,96 96,82 62,48');
			svg.appendChild(polygon);
			svg.addEventListener('click', self.hide);
			this.formElement.appendChild(svg);


			// add success and error blocks
			var error = document.createElement( "div" );
			error.className = "error";
			this.formElement.appendChild(error);
			var success = document.createElement( "div" );
			success.className = "success";
			this.formElement.appendChild(success);

			// add form elements
			for(var element in this.config.form){
				for(var i = 0; i < this.config.form[element].length; ++i){
					switch(element){
						case 'label':
							this.formElement.insertBefore(this.build(element, this.config.form[element][i]), this.formElement.querySelector('#' + this.config.form[element][i].for));
							break;
						default:
							this.formElement.appendChild(this.build(element, this.config.form[element][i]) );
					}
				}
			}

			// Validation and submit
			this.formElement.addEventListener('submit', function(event){
				event.preventDefault();

				// add hidden data
				self.formElement.appendChild(self.build('input', {type: 'hidden', name:'action', value:self.config.action}));
				for(var actionData in self.config.actionData){
					self.formElement.appendChild(self.build('input', {type: 'hidden', name:'actionData['+ actionData +']', value: self.config.actionData[actionData]}));
				}

				if(self.validate()){
					self.send();
				}
			})

		}

		this.build = function(tag, element){
			var htmlElement = document.createElement(tag);
			for(var name in element){
				switch(typeof element[name]){
					case 'string':
						htmlElement.setAttribute(name ,element[name]); break;
					case 'function':
						if(name.search(/^on/) === 0){
							htmlElement.addEventListener(name.replace(/^on/, '').toLowerCase(), function(){element[name](htmlElement);});
						}else{
							element[name](htmlElement);
						}
						break;
				}
			}
			return htmlElement;
		}

		this.addEvents = function(){
			for (var name in this.config.selector){
				for(var i = 0; i < this.config.selector[name].length; ++i){
					for (var k = 0; k < document.querySelectorAll(this.config.selector[name][i]).length; ++k){
						document.querySelectorAll(this.config.selector[name][i])[k].addEventListener(name, function(){
							self.show();
						});
					}
				}
			}
		}

		this.validate = function(){
			if(typeof self.config.validation == 'function'){
				return self.config.validation == true;
			}
			return true;
		};

		this.getFormData = function(){
			var data = {};
			for(var i = 0; i < self.formElement.querySelectorAll('select, input, textarea').length; ++i){
				if(typeof self.formElement.querySelectorAll('select, input, textarea')[i].value == 'string' && self.formElement.querySelectorAll('select, input, textarea')[i].value){
					data[self.formElement.querySelectorAll('select, input, textarea')[i].name] = self.formElement.querySelectorAll('select, input, textarea')[i].value;
				}else if(typeof self.formElement.querySelectorAll('select, input, textarea')[i].textContent == 'string'){
					data[self.formElement.querySelectorAll('select, input, textarea')[i].name] = self.formElement.querySelectorAll('select, input, textarea')[i].textContent;
				}
			}
			return data;
		}

		this.send = function(){
			var postStr = '',
				postData = self.getFormData();

			for(var name in postData){
				postStr += name + '=' + encodeURIComponent(postData[name]) + '&';
			}

			if (window.XMLHttpRequest) { // Mozilla, Safari, IE7+ ...
				var xhttp = new XMLHttpRequest();
			} else if (window.ActiveXObject) { // IE 6 and older
				var xhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xhttp.open("POST", "/feedback/sender.php");
			xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			xhttp.onreadystatechange = function(){ return self.getResponse(this)};
			xhttp.send(postStr);
		}

		this.getResponse = function(ajax){
			if (ajax.readyState != 4){
				return;
			}
			if(ajax.status == 200 ){
				self.showSuccess();
			}else{
				self.showErrors('Ошибка');
			}
		}

		this.showSuccess = function(){
			self.formElement.querySelector('.error').style.display = 'none';
			self.formElement.querySelector('input[type="submit"]').style.display = 'none';
			self.formElement.querySelector('.success').style.display = 'block';
			var msg = document.createTextNode('Отправлено');
			self.formElement.querySelector('.success').appendChild(msg);
			setTimeout(self.hide, 1000);
		}

		this.showErrors = function(msg){
			self.formElement.querySelector('.error').style.display = 'block';
			msg = document.createTextNode(msg);
			self.formElement.querySelector('.error').appendChild(msg);
		}

		this.show = function(){
			self.formElement.style.display = 'block';
			self.wrapper.style.display = 'block';
		}

		this.hide = function(){
			self.formElement.style.display = 'none';
			self.wrapper.style.display = 'none';
		}
	}
});
